package com.example.springjwt.repositories;

import com.example.springjwt.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
    public User findUserByEmail(String email){
        return new User(email,"123456", "First", "second");
    }
}
