package com.example.springjwt.model.request;


public record LoginReq(String email, String password) { }
