package com.example.springjwt.model;

public record User(
        String email,
        String password,
        String firstName,
        String lastName
) {
    public User(String email, String password){
        this(email, password, "", "");
    }
}
